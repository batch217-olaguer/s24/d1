console.log("Hello World");

// [SECTION] Exponent Operator

const firstNum = 8 ** 2;
console.log("8**2=64 BEFORE ES6 UPDATES.");
console.log(firstNum);

// After ES6 Update
const secondNum = Math.pow(8, 2);
console.log("AFTER ES6 UPDATES");
console.log(secondNum);

const thirdNum = Math.pow(8,3);
console.log("8*8*8=512 , Math.pow(8,3)");
console.log(thirdNum);

// [SECTION] Template Literals
/*
	- Allows us to write strings wihtout using the concatenation operator (+)
	- Greatly helps with code readability
*/

let name = "John";

// pre-template literal strings
let message = "Hello " + name + "! Welcom to Programming!";
console.log("Message without templaet literals: " + message);

// Using template literal
// we use backticks (``)
message = `Hello ${name}! Welcome to programming`;
console.log(`Message with template literals ${message}`);

// Multi-line using template literals
const anotherMessage = `${name} attended a math competition. 
He won it by solving the problem 8**2 with solution of ${firstNum}.`
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

// [SECTION] Array Destructuring
/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	helps with code readability
	- syntax
		let/const [variableName, variableName, variableName, VariableName] = array;
*/

const fullName = ["Juan", "Dela", "Cruz",];
console.log(fullName[0]); // Juan
console.log(fullName[1]); // Dela
console.log(fullName[2]); // Cruz

// Using template literal
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);
// console.log(`Hello ${firstName} ${middleName} ${lastName}`);

// Array Destructuring
const [firstName, middleName, lastName] = fullName;
console.log(`Hello ${firstName} ${middleName} ${lastName}`);

// object Destructuring.
/*
	- Allows to unpack propertiers of objects into distinct variables.
	- Shortens the syntax for accessing properties from objects
	- Syntax
		let/const {propertyName, propertyName, propertyName} = object;
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

// Preobject Destructuring
console.log("Pre-Object destructuring")
console.log(person.givenName); //Jane
console.log(person.maidenName); //Dela
console.log(person.familyName); //Cruz


console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName} It's good to see you again!`);

// Using Object Destructuring
const {givenName, maidenName, familyName} = person
console.log("Using object destructuring")
console.log(`Hello ${givenName} ${maidenName} ${familyName} It's good to see you again!`);

// using destructured variable to a function
function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}
console.log("using destructured variable to a function");
getFullName(person);

// [SECTION] Arrow Fucntions
/*
	- SYNTAX
		const variableName = () => {
	console.log
		}
*/


const hello = () => {
	console.log("Hello World!");
}
console.log(`Arrow Function result:`);
hello();

// Traditional Function
/*
	- SYNTAX
		function functionName(parameterA, parameterB, parameterC){
		console.log();
		}
*/

/*function printFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}
printFullName("John", "Doe", "Smith");*/

// Arrow Function
const printFullName1 = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`);
}

printFullName1("John", "Doe", "Smith");

const students = ["John", "Jane", "Judy"];

// Arrow Function with loops
// Pre-Arrow Function

students.forEach(function(student){
	console.log("Traditional Funciton")
	console.log(`${student} is a student`);
});

// Arrow Function
students.forEach((student) =>{
	console.log("Arrow Funtion usage.")
	console.log(`${student} is a student`);
})

const greet = (name = "User") => {
	return `Good morning, ${name}`
}
console.log(greet);
console.log(greet("Al"));
console.log(greet());

// Creating a class
/*
Syntax
class className{
	constructor(objectPropertyA, objectPropertyB) {
	this.objectPropertyA = objectPropertyA;
	this.objectPropertyB = objectPropertyB;
	}
}
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand,
		this.name = name,
		this.year = year
	}
}
console.log("class Car invoke no arguments.")
const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = "2021";

console.log("myCar with arguments.")
console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", "2022");
console.log("Same as sa taas.")
console.log(myNewCar);
